# user picks 6 numbers
# Lottery picks 6 numbers
# Match lotter vs user
# Calc winnings
import random

def get_player_numbers():
    number_csv = input("Enter 6 numbers separated by commas: ")
    number_list = number_csv.split(",")
    integer_set = {int(number) for number in number_list}
    return integer_set

def create_lottery_numbers():
    values = set()
    while len(values) < 6:
        values.add(random.randint(1,20))
    return values

def menu():
    user_numbers = get_player_numbers()
    lottery_numbers = create_lottery_numbers()
    matched_numbers = user_numbers.intersection(lottery_numbers)  
    print("You Won ${}!".format(100 ** len(matched_numbers)))
    print("lottery numbers {}".format(lottery_numbers))
    print("You Matched {} ".format(matched_numbers))


menu()
